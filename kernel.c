//#include "src/screen.h"
//#include "src/keyboard.h"
#include <stddef.h>
#include <stdint.h>

//basic checks to make sure about using x86-elf cross-compiler correctly
#if defined(__linux__)
	#error "This code must be compiled with a cross-compiler"
#elif !defined(__i386__)
	#error "This code must be compiled with an x86-elf compiler"
#endif

#define KBKEYLIST_H
#define KB_ESCAPE 0x01
#define KB_1 0x02
#define KB_2 0x03
#define KB_3 0x04
#define KB_4 0x05
#define KB_5 0x06
#define KB_6 0x07
#define KB_7 0x08
#define KB_8 0x09
#define KB_9 0x0A
#define KB_0 0x0B
#define KB_SYMBOL1 0x0C /* - */
#define KB_SYMBOL2 0x0D /* = */
#define KB_BACKSPACE 0x0E
#define KB_TAB 0x0F
#define KB_Q 0x10
#define KB_W 0x11
#define KB_E 0x12
#define KB_R 0x13
#define KB_T 0x14
#define KB_Y 0x15
#define KB_U 0x16
#define KB_I 0x17
#define KB_O 0x18
#define KB_P 0x19
#define KB_SYMBOL3 0x1A /* [ */
#define KB_SYMBOL4 0x1B /* ] */
#define KB_ENTER 0x1C
#define KB_LCONTROL 0x1D
#define KB_A 0x1E
#define KB_S 0x1F
#define KB_D 0x20
#define KB_F 0x21
#define KB_G 0x22
#define KB_H 0x23
#define KB_J 0x24
#define KB_K 0x25
#define KB_L 0x26
#define KB_SYMBOL5 0x27 /* ; */
#define KB_SYMBOL6 0x28 /* ' */
#define KB_SYMBOL7 0x29 /* ` */
#define KB_LSHIFT 0x2A
#define KB_SYMBOL8 0x2B /* \ */
#define KB_Z 0x2C
#define KB_X 0x2D
#define KB_C 0x2E
#define KB_V 0x2F
#define KB_B 0x30
#define KB_N 0x31
#define KB_M 0x32
#define KB_SYMBOL9 0x33 /* , */
#define KB_SYMBOL10 0x34 /* . */
#define KB_SYMBOL11 0x35 /* / */
#define KB_RSHIFT 0x36
#define KB_NUMPAD_MULT 0x37
#define KB_LALT 0x38
#define KB_SPACE 0x39
#define KB_CAPSLOCK 0x3A
#define KB_F1 0x3B
#define KB_F2 0x3C
#define KB_F3 0x3D
#define KB_F4 0x3E
#define KB_F5 0x3F
#define KB_F6 0x40
#define KB_F7 0x41
#define KB_F8 0x42
#define KB_F9 0x43
#define KB_F10 0x44
#define KB_NUMLOCK 0x45
#define KB_SCROLLLOCK 0x46
#define KB_NUMPAD7 0x47
#define KB_NUMPAD8 0x48
#define KB_NUMPAD9 0x49
#define KB_NUMPAD_MINUS 0x4A
#define KB_NUMPAD4 0x4B
#define KB_NUMPAD5 0x4C
#define KB_NUMPAD6 0x4D
#define KB_NUMPAD_PLUS 0x4E
#define KB_NUMPAD1 0x4F
#define KB_NUMPAD2 0x50
#define KB_NUMPAD3 0x51
#define KB_NUMPAD0 0x52
#define KB_NUMPAD_DOT 0x53

#define KB_SYMBOL12 0x56
#define KB_F11 0x57
#define KB_F12 0x58

#define KB_NUMPAD_ENTER 0x9C
#define KB_RCONTROL 0x9D
#define KB_NUMPAD_DIV 0xB5
#define KB_ALTGR 0xB8
#define KB_HOME 0xC7
#define KB_UP 0xC8
#define KB_PAGEUP 0xC9
#define KB_LEFT 0xCB
#define KB_RIGHT 0xCD
#define KB_END 0xCF
#define KB_DOWN 0xD0
#define KB_PAGEDOWN 0xD1
#define KB_INSERT 0xD2
#define KB_DELETE 0xD3

// Receives a 8/16/32-bit value from an I/O location
static uint8_t inb(uint16_t port)
{
		uint8_t ret;
		asm volatile ( "inb %1, %0"
									 : "=a"(ret)
									 : "Nd"(port) );
		return ret;
}

// Sends a 8/16/32-bit value on a I/O location
static inline void outb(uint16_t port, uint8_t val)
{
		asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
		/* There's an outb %al, $imm8  encoding, for compile-time constant port numbers that fit in 8b.  (N constraint).
		 * Wider immediate constants would be truncated at assemble-time (e.g. "i" constraint).
		 * The  outb  %al, %dx  encoding is the only option for all other cases.
		 * %1 expands to %dx because  port  is a uint16_t.  %w1 could be used if we had the port number a wider C type */
}


// This is the x86's VGA textmode buffer. To display text, we write data to this memory location
volatile uint16_t* vga_buffer = (uint16_t*)0xB8000;
// By default, the VGA textmode buffer has a size of 80x25 characters
const int VGA_COLS = 80;
const int VGA_ROWS = 25;

//starts displaying text in the top-left of the screen (column = 0, row = 0)
int term_col = 0;
int term_row = 0;
uint8_t term_color = 0x0F; // Black background, White foreground

//The keyboard handling needed!!!
//This function allows to chose terminal color
/*void terminal_color(const char* str)
{
		for(size_t i=0; stri[i] != '\0', i++)	// Keep placing characters until we hit the null-terminating character ('\0')

}*/

// This function initiates the terminal by clearing it
void term_init()
{
	// Clear the textmode buffer
	for (int col = 0; col < VGA_COLS; col ++)
	{
		for (int row = 0; row < VGA_ROWS; row ++)
		{
			// The VGA textmode buffer has size (VGA_COLS * VGA_ROWS).
			// Given this, we find an index into the buffer for our character
			const size_t index = (VGA_COLS * row) + col;
			// Entries in the VGA buffer take the binary form BBBBFFFFCCCCCCCC, where:
			// - B is the background color
			// - F is the foreground color
			// - C is the ASCII character
			vga_buffer[index] = ((uint16_t)term_color << 8) | ' '; // Set the character to blank (a space character)
		}
	}
}

// This function places a single character onto the screen
void term_putc(char c)
{
	// Remember - we don't want to display ALL characters!
	switch (c)
	{
	case '\n': // Newline characters should return the column to 0, and increment the row
		{
			term_col = 0;
			term_row ++;
			break;
		}

	default: // Normal characters just get displayed and then increment the column
		{
			const size_t index = (VGA_COLS * term_row) + term_col; // Like before, calculate the buffer index
			vga_buffer[index] = ((uint16_t)term_color << 8) | c;
			term_col ++;
			break;
		}
	}

	// if we get past the last column? We need to reset the column to 0, and increment the row to get to a new line
	if (term_col >= VGA_COLS)
	{
		term_col = 0;
		term_row ++;
	}

	// if we get past the last row we need to reset both column and row to 0 in order to loop back to the top of the screen
	if (term_row >= VGA_ROWS)
	{
		term_col = 0;
		term_row = 0;
	}
}

// Prints an entire string onto the screen, only string given in the argument
void term_print(const char* str)
{
	for (size_t i = 0; str[i] != '\0'; i ++) // Keep placing characters until we hit the null-terminating character ('\0')
		term_putc(str[i]);
}

//Provides the scancode from kb controller
//volatile uint16_t* kbuff = (uint16_t*)0x60;
char getScancode(){
	char c=0;
	do {
	if(inb(0x60) != c){
		c=inb(0x60);
		if(c>0)
		outb(0x60, 0x0);
		return c;
		}
	}while(1);
	}


//transfroms scancodes to chars
char getchar()
{
while(1)
switch (getScancode()) {
			case KB_1:	return '1';
				break;
			case KB_2:	return '2';
				break;
			case KB_3:	return '3';
				break;
			case KB_4: 	return '4';
				break;
			case KB_5: 	return '5';
				break;
			case KB_6:	return '6';
				break;
			case KB_7: 	return 'X';
				break;
			case KB_8: 	return '8';
				break;
			case KB_9: 	return '9';
				break;
			case KB_0: 	return '0';
				break;
			case KB_Q: 	return 'Q';
				break;
			case KB_W: 	return 'W';
				break;
			case KB_E: 	return 'E';
				break;
			case KB_R: 	return 'R';
				break;
			case KB_T: 	return 'T';
				break;
			case KB_Y: 	return 'Y';
				break;
			case KB_U: 	return 'U';
				break;
			case KB_I: 	return 'I';
				break;
			case KB_O: 	return 'O';
				break;
			case KB_P: 	return 'P';
				break;
			case KB_A: 	return 'A';
				break;
			case KB_S: 	return 'S';
				break;
			case KB_D: 	return 'D';
				break;
			case KB_F: 	return 'F';
				break;
			case KB_G: 	return 'G';
				break;
			case KB_H: 	return 'H';
				break;
			case KB_J: 	return 'J';
				break;
			case KB_K:	return 'K';
				break;
			case KB_L:	return 'L';
				break;
			case KB_Z: 	return 'Z';
				break;
			case KB_X: 	return 'X';
				break;
			case KB_C:	return 'C';
				break;
			case KB_V: 	return 'V';
				break;
			case KB_B:	return 'B';
				break;
			case KB_N: 	return 'N';
				break;
			case KB_M: 	return 'M';
	}
}

void kb_print() //shows the character on display
{
	char chara = getchar(); // Pressed key value
	term_putc(chara);
}

void kernel_main()
{
	term_init();

	term_print("It works!!!\n");

	while(1){
	kb_print();
	}
}
