.extern kernel_main

.global start

.set MB_MAGIC, 0x1BADB002          // This is a 'magic' constant that GRUB will use to detect kernel's location.
.set MB_FLAGS, (1 << 0) | (1 << 1)
.set MB_CHECKSUM, (0 - (MB_MAGIC + MB_FLAGS))

.section .multiboot
	.align 4
	.long MB_MAGIC
	.long MB_FLAGS
	.long MB_CHECKSUM

.section .bss
	.align 16
	stack_bottom:
		.skip 1024
	stack_top:

.section .text
	start:
		mov $stack_top, %esp   // Set the stack pointer to the top of the stack

		call kernel_main

		hang:
			cli
			hlt
			jmp hang
